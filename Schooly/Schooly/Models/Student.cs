﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Schooly.Models
{
    public class Student
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstMidName { get; set; }
        public int Age { get; set; }
        public string City { get; set; }
        public int YearsInSchool { get; set; }
        public DateTime EnrollmentDate { get; set; }
        [DisplayFormat(NullDisplayText ="No entry yet")]
        public double? Gpa { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }

    public class StudentDTO
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstMidName { get; set; }
        public int Age { get; set; }
        public string City { get; set; }
        public int YearsInSchool { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public double? Gpa { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}