﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Schooly.Models
{
    public class AssignmentViewModel
    {
        public string CourseName { get; set; }
        public int NumOfStudents { get; set; }
        public List<StudentInfo> StudentsNames { get; set; }
    }

    public class StudentInfo
    {
        public string FName { get; set; }
        public string LName { get; set; }
    }
}