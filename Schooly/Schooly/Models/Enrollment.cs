﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Schooly.Models
{
    public enum Grade
    {
        A, B, C, D, F
    }

    public class Enrollment
    {
        public int EnrollmentID { get; set; }
        public int CourseID { get; set; }
        public int StudentID { get; set; }
        // The '?' lets the value be nullable - not equal to zero (made the test - got 0)
        public Grade? Grade { get; set; }

        public virtual Course Course { get; set; }
        public virtual Student Student { get; set; }
    }
}