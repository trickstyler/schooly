﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace Schooly.Models
{
    public class Course
    {
        //the DatabaseGenerated attribute lets you enter
        // the primary key for the course rather than having the database generate it
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CourseID { get; set; }
        public string Title { get; set; }
        public int Credits { get; set; }
        public int Price { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}