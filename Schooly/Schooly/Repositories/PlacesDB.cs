﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Schooly.Models;


namespace Schooly.DAL
{
    class PlacesDB
    {
        public static IEnumerable<PlacesInSchool> GetSchoolPlaces()
        {
            using (var context = new SchoolContext())
            {
                var dbPlaces = context.Places
                    .ToArray(); // Run the query and avoid context dispose

                return dbPlaces;//.Select(CreateBranchModel)
                   // .ToArray();
            }
        }

        //private static PlacesInSchool CreateBranchModel(PlacesInSchool dbPlaces)
        //{
        //    return new PlacesInSchool
        //    {
        //        Id = dbPlaces.Id,
        //        PlaceName = dbPlaces.PlaceName,
        //        GeoLong = dbPlaces.GeoLong,
        //        GeoLat = dbPlaces.GeoLat
        //    };
        //}
    }
}
