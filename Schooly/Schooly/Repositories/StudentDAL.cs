﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Schooly.Models;
using System.Linq.Expressions;

namespace Schooly.DAL
{
    public class StudentDAL
    {
        //private static Expression<Func<Student,StudentDTO>> StudentToStudentDTO()
        //{
        //    return (x => new StudentDTO { ID = x.ID, FirstMidName = x.FirstMidName, LastName = x.LastName, Age = x.Age, City = x.City, EnrollmentDate = x.EnrollmentDate, Enrollments = x.Enrollments, Gpa = x.Gpa, YearsInSchool = x.YearsInSchool });
        //}

        private static List<Student> GetAll()
        {
            SchoolContext db = new SchoolContext();

            return db.Students.ToList<Student>();
        }

        public static IEnumerable<Student> GetFilteredStudents(string firstName, string lastName, double? gpa, int? yearsInSchool)
        {
            SchoolContext db = new SchoolContext();

            db.Configuration.ProxyCreationEnabled = false;

            IQueryable<Student> dbStudentsQuery = db.Students.AsQueryable();

            if (!string.IsNullOrEmpty(firstName))
            {
                dbStudentsQuery = dbStudentsQuery.Where(x => x.FirstMidName.Contains(firstName));
            }
            if (!string.IsNullOrEmpty(lastName))
            {
                dbStudentsQuery = dbStudentsQuery.Where(x => x.LastName.Contains(lastName));
            }
            if (gpa != null)
            {
                dbStudentsQuery = dbStudentsQuery.Where(x => x.Gpa == gpa);
            }
            if (yearsInSchool != null)
            {
                dbStudentsQuery = dbStudentsQuery.Where(x => x.YearsInSchool == yearsInSchool);
            }
            //if (customerId != null)
            //{
            //    dbStudentsQuery = dbStudentsQuery.Where(x => x.BookToCostumers.Any(y => y.CostumerId == customerId));
            //}

            return dbStudentsQuery.ToList();
        }
    }
}