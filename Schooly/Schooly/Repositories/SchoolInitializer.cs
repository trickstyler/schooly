﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Schooly.Models;

namespace Schooly.DAL
{

    // This is a SEED Method for development ONLY => it will populate
    // the DB as it's out of sync with the model
    // TODO: Delete or comment before prod
    public class SchoolInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<SchoolContext> //DropCreateDatabaseAlways<SchoolContext> //
    {
        protected override void Seed(SchoolContext context)
        {
            var students = new List<Student>
            {
            new Student{FirstMidName="Carson",LastName="Alexander",Age= 12,City="Ramat-Gan", YearsInSchool=2,EnrollmentDate=DateTime.Parse("2005-09-01")},
            new Student{FirstMidName="Meredith",LastName="Alonso",Age= 14,City="Tel-Aviv", YearsInSchool=4,EnrollmentDate=DateTime.Parse("2002-09-01")},
            new Student{FirstMidName="Arturo",LastName="Anand",Age= 12,City="Ramat-Gan", YearsInSchool=3,EnrollmentDate=DateTime.Parse("2003-09-01")},
            new Student{FirstMidName="Gytis",LastName="Barzdukas",Age= 18,City="Ashdod", YearsInSchool=5,EnrollmentDate=DateTime.Parse("2002-09-01")},
            new Student{FirstMidName="Yan",LastName="Li",Age= 12,City="Ramat-Gan", YearsInSchool=2,EnrollmentDate=DateTime.Parse("2002-09-01")},
            new Student{FirstMidName="Peggy",LastName="Justice",Age= 12,City="Ramat-Gan", YearsInSchool=2,EnrollmentDate=DateTime.Parse("2001-09-01")},
            new Student{FirstMidName="Laura",LastName="Norman",Age= 18,City="Ramat-Gan", YearsInSchool=2,EnrollmentDate=DateTime.Parse("2003-09-01")},
            new Student{FirstMidName="Nino",LastName="Olivetto",Age= 15,City="Netanya", YearsInSchool=5,EnrollmentDate=DateTime.Parse("2005-09-01")}
            };

            students.ForEach(s => context.Students.Add(s));
            context.SaveChanges();
            var courses = new List<Course>
            {
            new Course{CourseID=1050,Title="Chemistry",Credits=3,},
            new Course{CourseID=4022,Title="Microeconomics",Credits=3,},
            new Course{CourseID=4041,Title="Macroeconomics",Credits=3,},
            new Course{CourseID=1045,Title="Calculus",Credits=4,},
            new Course{CourseID=3141,Title="Trigonometry",Credits=4,},
            new Course{CourseID=2021,Title="Composition",Credits=3,},
            new Course{CourseID=2042,Title="Literature",Credits=4,}
            };
            courses.ForEach(s => context.Courses.Add(s));
            context.SaveChanges();
            var enrollments = new List<Enrollment>
            {
            new Enrollment{StudentID=1,CourseID=1050,Grade=Grade.A},
            new Enrollment{StudentID=1,CourseID=4022,Grade=Grade.C},
            new Enrollment{StudentID=1,CourseID=4041,Grade=Grade.B},
            new Enrollment{StudentID=2,CourseID=1045,Grade=Grade.B},
            new Enrollment{StudentID=2,CourseID=3141,Grade=Grade.F},
            new Enrollment{StudentID=2,CourseID=2021,Grade=Grade.F},
            new Enrollment{StudentID=3,CourseID=1050},
            new Enrollment{StudentID=4,CourseID=1050,},
            new Enrollment{StudentID=4,CourseID=4022,Grade=Grade.F},
            new Enrollment{StudentID=5,CourseID=4041,Grade=Grade.C},
            new Enrollment{StudentID=6,CourseID=1045},
            new Enrollment{StudentID=7,CourseID=3141,Grade=Grade.A},
            };
            enrollments.ForEach(s => context.Enrollments.Add(s));
            context.SaveChanges();

            var place = new List<PlacesInSchool>
            {
            new PlacesInSchool {Id=1, PlaceName="Library", GeoLong="34.7698510", GeoLat="31.9850610" },
            new PlacesInSchool {Id=2, PlaceName="School", GeoLong="34.6790510", GeoLat="31.8850610" },
            new PlacesInSchool {Id=3, PlaceName="Offices", GeoLong="34.6698510", GeoLat="31.6850610" },
            new PlacesInSchool {Id=4, PlaceName="Garden", GeoLong="34.6598510", GeoLat="31.7750610" },
            new PlacesInSchool {Id=5, PlaceName="Dani", GeoLong="34.6628510", GeoLat="31.7750610" }
            };
            place.ForEach(s => context.Places.Add(s));
            context.SaveChanges();
        }
    }
}