﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Schooly.Models;
using Schooly.DAL;

namespace Schooly.Repositories
{
    public class CalculusAgeDB
    {
        public static IEnumerable<Student> GetAllstudents()
        {
            using (var context = new SchoolContext())
            {
                var dbStudents = context.Students
                    .ToArray(); // Run the query and avoid context dispose

                return dbStudents;
            }
        }

        public static IEnumerable<Enrollment> GetAllEnrollments()
        {
            using (var context = new SchoolContext())
            {
                var dbEnroll = context.Enrollments
                    .ToArray(); // Run the query and avoid context dispose

                return dbEnroll;
            }
        }

        public static IEnumerable<Enrollment> GetCalculEnroll()
        {
            using (var context = new SchoolContext())
            {
                var dbEnrollment = context.Enrollments
                    .ToArray(); // Run the query and avoid context dispose

                List<Enrollment> CalcuList = new List<Enrollment>();

                foreach (var enrol in dbEnrollment)
                {
                    if (enrol.CourseID == 1045)
                    {
                        CalcuList.Add(enrol);
                    }
                }

                CalcuList.ToArray();
                return CalcuList;
            }
        }

        public static IEnumerable<Student> GetCity()
        {
            using (var context = new SchoolContext())
            {
                var dbStudents = context.Students
                    .ToArray(); // Run the query and avoid context dispose

                return dbStudents;
            }
        }

        public string joinTry()
        {
            var context = new SchoolContext();

            return "aaaa";
        }
    }
}