﻿using Schooly.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Schooly.DAL
{
    public class SchoolContext : DbContext
    {
        // "SchoolContext" = the connection string to the
        // DB. the connection string itself is stored in
        // the web.config file
        public SchoolContext() : base("SchoolContext")
        {
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<PlacesInSchool> Places { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // We're using the singular naming convention in our code
            // as opposed to the pluralized naming convention (Student != Student)
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}