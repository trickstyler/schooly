﻿using Schooly.DAL;
using Schooly.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Schooly.Repositories
{
    public class CourseDAL
    {
        public static IEnumerable<Course> GetFilteredCourses(string title = null, int? credits = null, int? price = null)
        {
            SchoolContext db = new SchoolContext();

            db.Configuration.ProxyCreationEnabled = false;

            IQueryable<Course> dbCoursesQuery = db.Courses.AsQueryable();

            if (!string.IsNullOrEmpty(title))
            {
                dbCoursesQuery = dbCoursesQuery.Where(x => x.Title.Contains(title));
            }
            if (credits != null)
            {
                dbCoursesQuery = dbCoursesQuery.Where(x => x.Credits == credits);
            }
            if (price != null)
            {
                dbCoursesQuery = dbCoursesQuery.Where(x => x.Price == price);
            }
            //if (customerId != null)
            //{
            //    dbCoursesQuery = dbCoursesQuery.Where(x => x.BookToCostumers.Any(y => y.CostumerId == customerId));
            //}

            return dbCoursesQuery.ToList();
        }
    }
}