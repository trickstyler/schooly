﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Schooly.DAL;
using Schooly.Models;
using Microsoft.Ajax.Utilities;
using System.Web.Script.Serialization;

namespace Schooly.Controllers
{
    public class StudentController : Controller
    {
        private SchoolContext db = new SchoolContext();

        private readonly JavaScriptSerializer _jsonSerializer;

        private void calculateGpa(Student student)
        {
            double curScore = 0;
            int totalCredits = 0;


            foreach (Enrollment curEnroll in student.Enrollments)
            {
                if (curEnroll.Grade != null)
                {
                    curScore += getNumGrade(curEnroll.Grade) * curEnroll.Course.Credits;
                    totalCredits += curEnroll.Course.Credits;
                }
            }

            if (student.Enrollments != null)
            {
                student.Gpa = Math.Round(curScore / totalCredits, 2);
            }
        }

        private double getNumGrade(Grade? curGrade)
        {
            double retVal = 0;

            switch (curGrade)
            {
                case Grade.A:
                    retVal = 4.0;
                    break;

                case Grade.B:
                    retVal = 3.0;
                    break;

                case Grade.C:
                    retVal = 2.0;
                    break;

                case Grade.D:
                    retVal = 1.0;
                    break;

                case Grade.F:
                    retVal = 0.0;
                    break;
                default:
                    break;
            }

            return (retVal);
        }

        private SelectList GetDistinctFNameList(List<Student> list)
        {
            // Creating a distinct (by first name) student list
            List<Student> distintStudents =
                list.Distinct<Student>(new CompareStudents()).ToList<Student>();

            // Creating the list items for the student drop down list
            IEnumerable<SelectListItem> studentListItems =
                distintStudents.AsEnumerable().Select(
                c => new SelectListItem
                {
                    Text = c.FirstMidName,
                    Value = c.ID.ToString(),
                    Selected = false
                });

            // Creating the select list
            SelectList studentsList = new SelectList(studentListItems, "Value", "Text");

            return studentsList;
        }

        private SelectList GetDistinctGpaList(List<Student> list)
        {
            // Creating a distinct (by Gpa) student list
            List<Student> distintGpaStudents =
                list.DistinctBy(x => x.Gpa).ToList<Student>();

            // Creating the list items for the student drop down list
            IEnumerable<SelectListItem> studentGpaListItems =
                distintGpaStudents.AsEnumerable().Select(
                c => new SelectListItem
                {
                    Text = c.Gpa.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                });

            // Creating the select list
            SelectList studentsList = new SelectList(studentGpaListItems, "Value", "Text");

            return studentsList;
        }

        private SelectList GetDistinctCityList(List<Student> list)
        {
            // Creating a distinct (by City) student list
            List<Student> distintCityStudents =
                list.DistinctBy(x => x.City).ToList<Student>();

            // Creating the list items for the student drop down list
            IEnumerable<SelectListItem> studentCityListItems =
                distintCityStudents.AsEnumerable().Select(
                c => new SelectListItem
                {
                    Text = c.City.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                });

            // Creating the select list
            SelectList studentsList = new SelectList(studentCityListItems, "Value", "Text");

            return studentsList;
        }

        public StudentController()
        {
            _jsonSerializer = new JavaScriptSerializer();
            ViewBag.IsAdminLogged = System.Web.HttpContext.Current.Session["IsAdminLogged"];
        }

        public string GetStudents(string firstName = null, string lastName = null, double? gpa = null, int? yearsInSchool = null)
        {
            return _jsonSerializer.Serialize(StudentDAL.GetFilteredStudents(firstName, lastName, gpa, yearsInSchool));
        }

        // GET: Student
        public ActionResult Index(string searchString)
        {
            var students = from s in db.Students
                           select s;

            List<Student> retVal = db.Students.ToList();

            if (!String.IsNullOrEmpty(searchString))
            {
                students = students.Where(s => s.LastName.Contains(searchString)
                                       || s.FirstMidName.Contains(searchString)
                                       || s.City.Contains(searchString)
                                       || s.Age.ToString().Contains(searchString));

                retVal = students.ToList<Student>();
            }

            foreach (Student curStudent in retVal)
            {
                calculateGpa(curStudent);
            }

            // Getting the Distinct student list
            SelectList studentsFNameList = GetDistinctFNameList(retVal);

            // Inserting the studentList to the View Bag
            ViewBag.StudentsFNameList = studentsFNameList;

            // Getting the Distinct gpa list
            SelectList studentGpaList = GetDistinctGpaList(retVal);

            // Inserting the gpa list to the View Bag
            ViewBag.GpaList = studentGpaList;

            // Getting the Distrinct city list
            SelectList studentCityList = GetDistinctCityList(retVal);

            // Inserting the city list the View Bag
            ViewBag.CityList = studentCityList;

            return View(retVal);
        }

        // GET: Student/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // GET: Student/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Student/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,LastName,FirstMidName,Age,City,YearsInSchool,EnrollmentDate")] Student student)
        {
            if (ModelState.IsValid)
            {
                db.Students.Add(student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(student);
        }

        // GET: Student/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,LastName,FirstMidName,Age,City,YearsInSchool,EnrollmentDate")] Student student)
        {
            if (ModelState.IsValid)
            {
                db.Entry(student).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(student);
        }

        [HttpPost]
        public ActionResult UpdateTable()
        {
            return View();
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Student/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Student student = db.Students.Find(id);
            db.Students.Remove(student);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }

    public class CompareStudents : IEqualityComparer<Student>
    {
        public bool Equals(Student x, Student y)
        {
            return x.FirstMidName == y.FirstMidName;
        }

        public int GetHashCode(Student codeh)
        {
            return codeh.GetHashCode();
        }
    }
}
