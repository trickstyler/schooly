﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Schooly.DAL;
using Schooly.Models;

namespace Schooly.Controllers
{
    public class StatisticsController : Controller
    {
        private SchoolContext db = new SchoolContext();

        private void calculateGpa(Student student)
        {
            double curScore = 0;
            int totalCredits = 0;


            foreach (Enrollment curEnroll in student.Enrollments)
            {
                if (curEnroll.Grade != null)
                {
                    curScore += getNumGrade(curEnroll.Grade) * curEnroll.Course.Credits;
                    totalCredits += curEnroll.Course.Credits;
                }
            }

            if (student.Enrollments != null)
            {
                student.Gpa = Math.Round(curScore / totalCredits, 2);
            }
        }

        private double getNumGrade(Grade? curGrade)
        {
            double retVal = 0;

            switch (curGrade)
            {
                case Grade.A:
                    retVal = 4.0;
                    break;

                case Grade.B:
                    retVal = 3.0;
                    break;

                case Grade.C:
                    retVal = 2.0;
                    break;

                case Grade.D:
                    retVal = 1.0;
                    break;

                case Grade.F:
                    retVal = 0.0;
                    break;
                default:
                    break;
            }

            return (retVal);
        }

        //GET: Statistics
        public ActionResult Index(string searchString)
        {
            var studentsAssignedToCourses =
                (from c in db.Courses
                 join e in db.Enrollments on c.CourseID equals e.CourseID
                 join s in db.Students on e.StudentID equals s.ID into q1
                 group q1 by c.Title into q2
                 select new AssignmentViewModel
                 {
                     CourseName = q2.Key,
                     NumOfStudents = q2.Count(),
                     StudentsNames =
                     (
                        from enr in db.Enrollments
                        where enr.Course.Title == q2.Key
                        select new StudentInfo
                        {
                            FName = enr.Student.FirstMidName,
                            LName = enr.Student.LastName
                        }).ToList()
                 }).ToList();

            #region OLD
            //from s in db.Students
            //    join e in db.Enrollments on s.ID equals e.StudentID
            //    join c in db.Courses on e.CourseID equals c.CourseID into q1
            //    group q1 by s.LastName

            //List<Student> retVal = students.ToList<Student>();

            //foreach (Student curStudent in retVal)
            //{
            //    calculateGpa(curStudent);
            //}

            //retVal = retVal.Distinct<Student>(new CompareStudents()).ToList<Student>(); 
            #endregion

            return View(studentsAssignedToCourses);
        }

        public ActionResult CalculusAge()
        {
            return View();
        }

        public ActionResult StudentCity()
        {
            return View();
        }

        //public string GetCalculsAges()
        //{
        //    return "age,population\r\n" + String.Join(Environment.NewLine, AuthorRepository.GetAllAuthors()
        //        .Select(x => x.Name + " - " + x.BooksWrittenBy.Count + "," + x.BooksWrittenBy.Count));
        //}

        //public string GetStudentsGradesCount()
        //{
        //    string a = "letter	frequency\r\n" + String.Join(Environment.NewLine, DAL.CalculusAgeDB.GetAllEnrollments().GroupBy(x => x.Grade)
        //          .Select(x => x.Key + "\t" + x.Count()));
        //    return a;
        //}
    }
}