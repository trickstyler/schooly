﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Schooly.DAL;
using Schooly.Models;

namespace Schooly.Controllers
{
    public class PlacesController : Controller
    {
        private SchoolContext db = new SchoolContext();

        private readonly JavaScriptSerializer _jsonSerializer;

        public PlacesController()
        {
            _jsonSerializer = new JavaScriptSerializer();
        }

        // GET: Places
        public ActionResult Index(string searchString)
        {
            var places = from s in db.Places
                          select s;

            List<PlacesInSchool> retVal = db.Places.ToList();

            if (!String.IsNullOrEmpty(searchString))
            {
                places = places.Where(s => s.PlaceName.Contains(searchString)
                                       || s.GeoLat.ToString().Contains(searchString)
                                       || s.GeoLong.ToString().Contains(searchString));

                retVal = places.ToList();
            }

            return View(retVal);
        }

        public string GetAllPlaces()
        {
            return _jsonSerializer.Serialize(DAL.PlacesDB.GetSchoolPlaces());
        }
    }
}