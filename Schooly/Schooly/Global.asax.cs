﻿using Schooly.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using WebMatrix.WebData;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Schooly
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private ApplicationSignInManager _signInManager;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            // SEEDING the DB after starting the app
            // TODO: delete or comment before Prod
            Database.SetInitializer(new SchoolInitializer());
            setAdmin();
        }

        public void setAdmin()
        { 
            //if (!WebSecurity.UserExists("admin@admin.com"))
            //    WebSecurity.CreateUserAndAccount("admin@admin.com", "Aa123456@");
        }
        
    }
}
