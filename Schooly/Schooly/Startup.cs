﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Schooly.Startup))]
namespace Schooly
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
